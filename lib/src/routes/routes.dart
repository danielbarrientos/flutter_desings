import 'package:disenos/src/pages/animaciones_page.dart';
import 'package:disenos/src/pages/emergency_page.dart';
import 'package:disenos/src/pages/graficas_circulares_pages.dart';
import 'package:disenos/src/pages/headers_page.dart';
import 'package:disenos/src/pages/launcher_pager.dart';
import 'package:disenos/src/pages/pinterest_page.dart';
import 'package:disenos/src/pages/slideshow_page.dart';
import 'package:disenos/src/pages/sliver_list_page.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


final pageRoutes = <_Route>[

  _Route(FontAwesomeIcons.circle, 'Animate', AnimacionesPage()),
  _Route(FontAwesomeIcons.slideshare, 'Slideshow', SlideshowPage()),
  _Route(FontAwesomeIcons.ambulance, 'Emergency', EmergencyPage()),
  _Route(FontAwesomeIcons.heading, 'Headings', HeadersPage()),
  _Route(FontAwesomeIcons.peopleCarry, 'Circular progress', GraficasCircularesPage()),
  _Route(FontAwesomeIcons.pinterest ,'Pinterest', PinterestPage()),
  _Route(FontAwesomeIcons.mobile, 'Sliver', SliverListPage()),
];


class _Route {
  final IconData icon;
  final String title;
  final Widget page;

  _Route(this.icon, this.title, this.page);

  
}
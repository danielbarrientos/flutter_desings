import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PinterestButtom {

  final IconData icon;
  final Function onPressed;
  

  PinterestButtom({
    @required this.icon, 
    @required this.onPressed
  });
}

class PinterestMenu extends StatelessWidget {

  final bool show;
  final Color backgroundColor;
  final Color activeColor;
  final Color inactiveColor;
  final List<PinterestButtom> items;

  PinterestMenu({
    this.show = true,
    this.backgroundColor = Colors.white,
    this.activeColor = Colors.black,
    this.inactiveColor = Colors.grey,
    @required this.items,
  });

  
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => new _MenuModel(),
      child: AnimatedOpacity(
        opacity: (show) ?1 :0,
        duration: Duration(milliseconds: 500),
        child: Builder(
          builder: (BuildContext context){
            Provider.of<_MenuModel>(context).backgroundColor = this.backgroundColor;
            Provider.of<_MenuModel>(context).activeColor = this.activeColor;
            Provider.of<_MenuModel>(context).inactiveColor = this.inactiveColor;

            return _PinterestMenuContainer(
              child: _MenuItems(items)
            );
          },
        ),
      ),
    );
  }
}

class _PinterestMenuContainer extends StatelessWidget {
  
  final Widget child;
  _PinterestMenuContainer({@required this.child});

  @override
  Widget build(BuildContext context) {

    final model = Provider.of<_MenuModel>(context);
    return Container(
      child: child,
      width: 240,
      height: 50,
      decoration: BoxDecoration(
        color: model.backgroundColor,
        borderRadius: BorderRadius.all(Radius.circular(100)),
        boxShadow: [
          BoxShadow(
            color: Colors.black38,
            blurRadius: 10,
            spreadRadius: -5,
          )
        ]
      ),
    );
  }
}


class _MenuItems extends StatelessWidget {
  final  List<PinterestButtom> menuItems;

  const _MenuItems(this.menuItems) ;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: List.generate(menuItems.length, (i) => _PinterestMenuButton(i,menuItems[i]))
    );
  }
}

class _PinterestMenuButton extends StatelessWidget {

  final int index;
  final PinterestButtom item;

  _PinterestMenuButton(this.index,this.item);

  @override
  Widget build(BuildContext context) {

    final itemSeleccionado = Provider.of<_MenuModel>(context).itemSeleccionado;
    final model = Provider.of<_MenuModel>(context);
    return GestureDetector(
      onTap: (){
         Provider.of<_MenuModel>(context,listen: false).itemSeleccionado= index;
         item.onPressed();
      },
      behavior: HitTestBehavior.translucent,
      child: AnimatedContainer(
        duration: Duration(milliseconds: 900),
        child: Icon(
          item.icon,
          size:  (itemSeleccionado == index)?35:30,
          color: (itemSeleccionado == index)? model.activeColor: model.inactiveColor,
        ),
      ),
    );
  }
}

class _MenuModel with ChangeNotifier {
  
  int _itemSeleccionado = 0;
  Color _backgroundColor = Colors.white;
  Color _activeColor= Colors.black;
  Color _inactiveColor = Colors.blueGrey;
  
  int get itemSeleccionado  => this._itemSeleccionado;

  set itemSeleccionado(int index) {
    this._itemSeleccionado = index;
    notifyListeners();
  }

    Color get backgroundColor => this._backgroundColor;

  set backgroundColor (Color value){
    this._backgroundColor = value;

  }

  Color get activeColor => this._activeColor;

  set activeColor (Color value){
    this._activeColor = value;

  }

  Color get inactiveColor => this._inactiveColor;

  set inactiveColor (Color value){
    this._inactiveColor = value;

  }
}
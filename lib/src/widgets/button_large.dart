
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ButtonLarge extends StatelessWidget {

  final IconData icon;
  @required final String title; 
  final Color colo1,colo2;
  @required final Function onPress;

  const ButtonLarge({
    this.icon = FontAwesomeIcons.plus, 
    this.title, 
    this.colo1 = Colors.grey, 
    this.colo2 = Colors.black, 
    this.onPress
  });

  @override
  Widget build(BuildContext context) {
     final colorWhite = Colors.white.withOpacity(0.8);
    return GestureDetector(
      
      onTap: this.onPress,
      child: Stack(
        children: [
          _ButtonLargeBackground(icon: this.icon, color1: this.colo1,color2: this.colo2,),
          
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 140, width: 40,),
              FaIcon(
               this.icon,
                size: 50,
                color: colorWhite,
              ),
              SizedBox(width: 20,),
              Expanded(child: Text(this.title,style: TextStyle(color:colorWhite ),)),
              FaIcon(FontAwesomeIcons.chevronRight,color: colorWhite,),
              SizedBox(width: 40,)
            ],
          )
        ],
      ),
    ); 
  }
}

class _ButtonLargeBackground extends StatelessWidget {
  
  final IconData icon;
  final Color color1,color2;

  const _ButtonLargeBackground({this.icon,this.color1,this.color2});
  @override
  Widget build(BuildContext context) {
   

    return Container(
      width: double.infinity,
      height: 100,
      margin: EdgeInsets.all( 20),
      decoration: BoxDecoration(
        color: Colors.red,
        boxShadow: [ BoxShadow(
            color: Colors.black.withOpacity(0.2),
            offset: Offset(4,6),
            blurRadius: 10
          )
        ],
        borderRadius: BorderRadius.circular(15),
        gradient: LinearGradient(
          colors: [
            this.color1,
            this.color2
          ] 
        )
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(15),
        child: Stack(
          children: [
            Positioned(
              right: -30,
              top: -20,
              child: FaIcon(
                this.icon,
                size: 150,
                color: Colors.white.withOpacity(0.2),
              ),
            ),
            
          ],
        ),
      ),
    );
  }
}
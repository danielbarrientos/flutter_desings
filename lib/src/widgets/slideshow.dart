import 'package:disenos/src/models/slider_model.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

class Slideshow extends StatelessWidget {

  final List<Widget> slides;
  final bool puntosArriba;
  final Color colorPrimario;
  final Color colorSecundario;
  final double bulletPrimario;
  final double bulletSecundario;


  Slideshow({
    @required this.slides,
    this.puntosArriba = false,
    this.colorPrimario = Colors.blue,
    this.colorSecundario = Colors.grey,
    this.bulletPrimario = 12,
    this.bulletSecundario = 12,
  });



  @override
  Widget build(BuildContext context) {
    
    return ChangeNotifierProvider(
      create: (_) => SliderModel(),
      child: SafeArea(
        child: Center(
          child: Builder(
            builder: (BuildContext context){

              Provider.of<SliderModel>(context).colorPrimario    = this.colorPrimario;
              Provider.of<SliderModel>(context).colorSecundario  = this.colorSecundario;
              Provider.of<SliderModel>(context).bulletPrimario   = this.bulletPrimario;
              Provider.of<SliderModel>(context).bulletSecundario = this.bulletSecundario;
              return Column(
                children: [
                  if(this.puntosArriba)  _Dots(this.slides.length),
                  _Slides(this.slides),
                  if(! this.puntosArriba)  _Dots(this.slides.length),
                ],
              );
            },
            
          )
        ),
      ),
    );
  }
}


class _Dots extends StatelessWidget {

  final  int totalSlides;

  const _Dots(this.totalSlides);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 70.0,
      // color: Colors.red,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: List.generate(totalSlides, (i) => _Dot(i)),
      ),
    );
  }
}

class _Dot extends StatelessWidget {

  final int index;


  const _Dot(@required this.index);
  
  @override
  Widget build(BuildContext context) {
    final model = Provider.of<SliderModel>(context);
    double sized;
    Color color;
    if(model.currentPage >= index - 0.5 &&  model.currentPage < index +0.5){
      sized = model.bulletPrimario;
      color = model.colorPrimario;
    }
    else{
      sized = model.bulletSecundario;
      color = model.colorSecundario;
    }

    return AnimatedContainer(
      duration: Duration(milliseconds: 300),
      width: sized,
      height: sized,
      margin: EdgeInsets.symmetric(horizontal: 5),
      decoration: BoxDecoration(
        color: color,
        shape: BoxShape.circle
      ),
    );
  }
}
class _Slides extends StatefulWidget {

  final List<Widget> slides;
  _Slides(this.slides);

  @override
  __SlidesState createState() => __SlidesState();
}

class __SlidesState extends State<_Slides> {

  final pageViewController = new PageController();

  @override
  void initState() {
     super.initState();

    pageViewController.addListener(() { 
      Provider.of<SliderModel>(context, listen: false).currentPage = pageViewController.page;
    });
    
    
  }

  @override
  void dispose() {
    pageViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Expanded(
      child: Container(
        child: PageView(
          controller: pageViewController,
          children: widget.slides.map((myslide) => _Slide(myslide)).toList(),
        ),
      ),
    );
  }
}

class _Slide extends StatelessWidget {

  final Widget slide;

  const _Slide(this.slide);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      padding: EdgeInsets.all(30),
      child: slide,
    );
  }
}


import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HeaderCuadrado extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      color: Color(0xff615AAB),
    );
  }
}

class HeaderBordesRedondeados extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      decoration: BoxDecoration(
         color: Color(0xff615AAB),
         borderRadius: BorderRadius.only(bottomLeft: Radius.circular(60), bottomRight: Radius.circular(60))
      ),
    );
  }
}

class HeaderDiagonal extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      // color: Color(0xff615AAB),
      child: CustomPaint(
        painter: _HeaderDiagonalPainter(),
      ),
      
    );
  }
}

class _HeaderDiagonalPainter extends CustomPainter{

  @override
  void paint(Canvas canvas, Size size) {
    final lapiz = Paint();
    //propiedades
    lapiz.color = Color(0xff615AAB);
    lapiz.style = PaintingStyle.fill; // stroke:linea //fill:relleno
    lapiz.strokeWidth = 10;

    final path = new Path();
    //dibujar con el path y el lapiz
    path.moveTo(0, size.height * 0.35);
    path.lineTo(size.width, size.height * 0.30);
    path.lineTo(size.width, 0);
    path.lineTo(0, 0);

    canvas.drawPath(path, lapiz);
  }
  
    @override
    bool shouldRepaint(covariant CustomPainter oldDelegate) {
      
      return true;
  }

}

class HeaderTriangular extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      // color: Color(0xff615AAB),
      child: CustomPaint(
        painter: _HeaderTriangularPainter(),
      ),
      
    );
  }
}

class _HeaderTriangularPainter extends CustomPainter{

  @override
  void paint(Canvas canvas, Size size) {
    final lapiz = Paint();
    //propiedades
    lapiz.color = Color(0xff615AAB);
    lapiz.style = PaintingStyle.fill; // stroke:linea //fill:relleno
    lapiz.strokeWidth = 10;

    final path = new Path();
    //dibujar con el path y el lapiz
    path.moveTo(0,0);
    path.lineTo(size.width, 0);
    path.lineTo(size.width, size.height);

    canvas.drawPath(path, lapiz);
  }
  
    @override
    bool shouldRepaint(covariant CustomPainter oldDelegate) {
      
      return true;
  }
}

class HeaderPico extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      // color: Color(0xff615AAB),
      child: CustomPaint(
        painter: _HeaderPicoPainter(),
      ),
      
    );
  }
}

class _HeaderPicoPainter extends CustomPainter{

  @override
  void paint(Canvas canvas, Size size) {
    final lapiz = Paint();
    //propiedades
    lapiz.color = Color(0xff615AAB);
    lapiz.style = PaintingStyle.fill; // stroke:linea //fill:relleno
    lapiz.strokeWidth = 10;

    final path = new Path();
    //dibujar con el path y el lapiz
    path.moveTo(0,0);
    path.lineTo(size.width, 0);
    path.lineTo(size.width, size.height *0.2);
    path.lineTo(size.width *0.5, size.height *0.3);
    path.lineTo(0, size.height *0.2);
    path.lineTo(0,0);

    canvas.drawPath(path, lapiz);
  }
  
  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
      
    return true;
  }
}

class HeaderCurvo extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      // color: Color(0xff615AAB),
      child: CustomPaint(
        painter: _HeaderCurvoPainter(),
      ),
      
    );
  }
}

class _HeaderCurvoPainter extends CustomPainter{

  @override
  void paint(Canvas canvas, Size size) {
    final lapiz = Paint();
    //propiedades
    lapiz.color = Color(0xff615AAB);
    lapiz.style = PaintingStyle.fill; // stroke:linea //fill:relleno
    lapiz.strokeWidth = 20;

    final path = new Path();
    //dibujar con el path y el lapiz
    path.lineTo(0, size.height * 0.25);
    path.quadraticBezierTo(size.width*0.5, size.height * 0.4, size.width, size.height * 0.25);
    path.lineTo(size.width, 0);
    // path.lineTo(0, 0);
    // path.lineTo(0, size.height * 0.25);
   

    canvas.drawPath(path, lapiz);
  }
  
  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
      
    return true;
  }
}

class HeaderWave extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      // color: Color(0xff615AAB),
      child: CustomPaint(
        painter: _HeaderWavePainter(),
      ),
      
    );
  }
}

class _HeaderWavePainter extends CustomPainter{

  @override
  void paint(Canvas canvas, Size size) {
    final lapiz = Paint();
    //propiedades
    lapiz.color = Color(0xff615AAB);
    lapiz.style = PaintingStyle.fill; // stroke:linea //fill:relleno
    lapiz.strokeWidth = 20;

    final path = new Path();
    //dibujar con el path y el lapiz
    

    path.lineTo(0, size.height * 0.30);
    path.quadraticBezierTo(size.width*0.25, size.height * 0.4, size.width *0.5, size.height * 0.30);
    path.quadraticBezierTo(size.width*0.75, size.height * 0.2, size.width, size.height * 0.30);
    path.lineTo(size.width, 0);
    canvas.drawPath(path, lapiz);
  }
  
  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
      
    return true;
  }
}


class HeaderWaveGradient extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      // color: Color(0xff615AAB),
      child: CustomPaint(
        painter: _HeaderWaveGradientPainter(),
      ),
      
    );
  }
}

class _HeaderWaveGradientPainter extends CustomPainter{

  @override
  void paint(Canvas canvas, Size size) {

    final Rect rect = new Rect.fromCircle(
      center: Offset(0.0,80.0),
      radius: 180
    );
    final Gradient gradiente = new LinearGradient(
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter,
      colors: <Color>[
        Color(0xff6D05E8),
        Color(0xffC012FF),
        Color(0xff6D05FA),
      ],
      stops: [
        0.4,
        0.5,
        1.0
      ]
    );

    final lapiz = Paint()..shader = gradiente.createShader(rect);
    //propiedades
    // lapiz.color = Color(0xff615AAB);
    lapiz.style = PaintingStyle.fill; // stroke:linea //fill:relleno
    lapiz.strokeWidth = 20;

    final path = new Path();
    //dibujar con el path y el lapiz
    
    path.lineTo(0, size.height * 0.30);
    path.quadraticBezierTo(size.width*0.25, size.height * 0.4, size.width *0.5, size.height * 0.30);
    path.quadraticBezierTo(size.width*0.75, size.height * 0.2, size.width, size.height * 0.30);
    path.lineTo(size.width, 0);
    canvas.drawPath(path, lapiz);
  }
  
  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
      
    return true;
  }
}

class IconHeader extends StatelessWidget {
  
  final IconData icon;
  final String titulo,subtitulo;
  final Color color1,color2;

  const IconHeader({
    @required this.icon, 
    @required this.titulo, 
    @required this.subtitulo, 
    this.color1=Colors.grey, 
    this.color2=Colors.blueGrey
  });

  @override
  Widget build(BuildContext context) {

    final Color colorWhite = Colors.white.withOpacity(0.8);
    return Stack(
      children: [
        _IconHeaderBackgound(color1: this.color1,color2: this.color2,),
        Positioned(
          top: -50,
          left: -70,
          child: FaIcon(
            this.icon,
            size: 250,
            color: Colors.white.withOpacity(0.2),
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: 90, width: double.infinity,),
            Text(this.subtitulo,style: TextStyle(color: colorWhite,fontSize: 15)),
            SizedBox(height: 20,),
            Text(this.titulo,style: TextStyle(color: colorWhite,fontSize: 22,fontWeight: FontWeight.w700)),
            SizedBox(height: 20,),
            FaIcon(this.icon,size: 90,color: colorWhite)
          ],
        )
      ],
    );
  }
}

class _IconHeaderBackgound extends StatelessWidget {
  
  final Color color1,color2;

  const _IconHeaderBackgound({
    @required this.color1,
    @required this.color2
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 300,
      decoration: BoxDecoration(
        // color: Colors.red,
        borderRadius: BorderRadius.only(bottomLeft: Radius.circular(80)),
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            this.color1,
            this.color2,
          ]
        )
      ),
    );
  }
}
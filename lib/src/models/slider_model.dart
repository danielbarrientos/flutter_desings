import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class SliderModel with ChangeNotifier {

  double _currentPage = 0;
  Color _colorPrimario = Colors.blue;
  Color _colorSecundario = Colors.grey;
  double _bulletPrimario = 12;
  double _bulletSecundario = 12;


  double get currentPage => this._currentPage;

  set currentPage (value){
    this._currentPage = value;
    notifyListeners();
  }

  Color get colorPrimario => this._colorPrimario;

  set colorPrimario (value){
    this._colorPrimario = value;
    
  }

  Color get colorSecundario => this._colorSecundario;

  set colorSecundario (value){
    this._colorSecundario = value;
    
  }

  double get bulletPrimario => this._bulletPrimario;

  set bulletPrimario (value){
    this._bulletPrimario = value;
    
  }

  double get bulletSecundario => this._bulletSecundario;

  set bulletSecundario (value){
    this._bulletSecundario = value;
    
  }
}
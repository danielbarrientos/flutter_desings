import 'package:flutter/material.dart';

class ThemeChanger with ChangeNotifier {

  bool _darkTheme = false;
  bool _customTheme = false;
  ThemeData _currentTheme;

  ThemeChanger(int theme){
    switch (theme) {
      case 1://light
        _darkTheme = false;
        _customTheme = false;
        _currentTheme = ThemeData.light();  
        break;
      case 2://dark
        _darkTheme = true;
        _customTheme = false;
        _currentTheme = ThemeData.dark();  
        break;
      case 3://custom
        _darkTheme = false;
        _customTheme = true;
        break;
      default:
         _darkTheme = false;
        _customTheme = false;
        _currentTheme = ThemeData.light();  
    }
  }

  bool get darkTheme => this._darkTheme;

  set darkTheme( bool value){
     this._customTheme = false;
    this._darkTheme = value;

    if(value)
      _currentTheme = ThemeData.dark();
    else{
       _currentTheme = ThemeData.light();
    }
   
    notifyListeners();
  }


  bool get customTheme => this._customTheme;

  set customTheme(bool value){
    this._customTheme = value;
    this._darkTheme = false;

    if(value)
      _currentTheme = ThemeData.dark().copyWith(
        accentColor: Color(0xff48A0EB),
        primaryColor: Colors.white,
        scaffoldBackgroundColor: Color(0xff16202B),
        textTheme: TextTheme(
          bodyText1: TextStyle(color: Colors.white)
        )
        
      );
    else{
      _currentTheme = ThemeData.light();
    }

    notifyListeners();
  }

  ThemeData get currentTheme => this._currentTheme;

}
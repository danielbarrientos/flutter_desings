import 'package:disenos/src/routes/routes.dart';
import 'package:disenos/src/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';


class LauncherPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
     final appTheme = Provider.of<ThemeChanger>(context);

    return Scaffold(
      
      appBar: AppBar(
        backgroundColor: (appTheme.darkTheme)? Colors.black: Colors.blue,
        title: Text('Diseños en flutter'),
      ),
      body: _OptionsList(),
      drawer: _MainMenu(),
   );
  }
}

class _OptionsList extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    
    final appTheme = Provider.of<ThemeChanger>(context).currentTheme; 

    return ListView.separated(
      // physics: BouncingScrollPhysics(),
      itemBuilder: (context,i) => ListTile(
        leading: FaIcon(pageRoutes[i].icon, color: appTheme.accentColor,),
        title: Text(pageRoutes[i].title),
        trailing: Icon(Icons.chevron_right, color: appTheme.accentColor,),
        onTap: (){

          Navigator.push(context, MaterialPageRoute(builder: (context)=> pageRoutes[i].page));
        },
      ), 
      separatorBuilder: (context,i)=> Divider(
        color: appTheme.primaryColorLight,
      ), 
      itemCount: pageRoutes.length
    );
  }
}

class _MainMenu extends StatelessWidget {


  @override
  Widget build(BuildContext context) {

    final appTheme = Provider.of<ThemeChanger>(context);
    final accentColor = appTheme.currentTheme.accentColor;

    return Drawer(
      child: Container(
        child: Column(
          children: [
            SafeArea(
              child: Container(
                width: double.infinity,
                height: 150,
                child: CircleAvatar(
                  backgroundColor: accentColor,
                  child: Text('D', style: TextStyle(fontSize: 50),),
                ),
              ),
            ),
            SizedBox(height: 5,),
            
            Expanded(
              child: _OptionsList()
            ),
            Divider(
              color: Colors.black38,
            ),
            ListTile(
              leading: Icon(Icons.lightbulb_outline,color:accentColor,),
              title: Text('Dark mode'),
              trailing: Switch.adaptive(
                value: appTheme.darkTheme, 
                onChanged: (value) => appTheme.darkTheme = value
                
              ),
            ),
            SafeArea(
              bottom: true,
              top: false,
              left: false,
              right: false,
              child: ListTile(
                leading: Icon(Icons.add_to_home_screen ,color:accentColor,),
                title: Text('Custom theme'),
                trailing: Switch.adaptive(
                  value: appTheme.customTheme, 
                  activeColor: accentColor,
                  onChanged: (value)=> appTheme.customTheme = value
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
import 'package:disenos/src/theme/theme.dart';
import 'package:disenos/src/widgets/slideshow.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

class SlideshowPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      // backgroundColor: Colors.deepPurple ,
      body: Column(
        children: [
           Expanded(child: _MySlideshow()),
           Expanded(child: _MySlideshow()),
        ],
      ),
    );
  }
}

class _MySlideshow extends StatelessWidget {
  const _MySlideshow({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    final appTheme = Provider.of<ThemeChanger>(context);
    final accentColor = appTheme.currentTheme.accentColor;

    return Slideshow(
      bulletPrimario:17,
      bulletSecundario: 14,
      colorPrimario: (appTheme.darkTheme)? accentColor: Colors.pink,
      slides: [
        SvgPicture.asset('assets/svg/slide1.svg'),
        SvgPicture.asset('assets/svg/slide2.svg'),
        SvgPicture.asset('assets/svg/slide3.svg'),
        SvgPicture.asset('assets/svg/slide4.svg'),
        SvgPicture.asset('assets/svg/slide5.svg'),
        SvgPicture.asset('assets/svg/slide6.svg'),
      ]
    );
  }
}


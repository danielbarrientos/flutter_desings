import 'package:disenos/src/theme/theme.dart';
import 'package:disenos/src/widgets/radial_progress.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class GraficasCircularesPage extends StatefulWidget {
  GraficasCircularesPage({Key key}) : super(key: key);

  @override
  _GraficasCircularesPageState createState() => _GraficasCircularesPageState();
}

class _GraficasCircularesPageState extends State<GraficasCircularesPage> {

  double porcentaje = 0.0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          setState(() {
            porcentaje += 10;
            if(porcentaje >=110){
               porcentaje= 0; 
            }
          });
        },
        child: Icon(Icons.play_arrow),
      ),
       body: Column(
         mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
            _CustomRadialProgress(porcentaje: porcentaje,color: Colors.pink, grosorPrimario: 8.0, grosorSecundario: 9.0,),
            _CustomRadialProgress(porcentaje: porcentaje *1.2,color: Colors.orange, grosorPrimario: 8.0, grosorSecundario: 9.0,),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
            _CustomRadialProgress(porcentaje: porcentaje *1.4,color: Colors.blueAccent, grosorPrimario: 8.0, grosorSecundario: 9.0,),
            _CustomRadialProgress(porcentaje: porcentaje *1.6,color: Colors.deepPurple, grosorPrimario: 8.0, grosorSecundario: 9.0,),
            ],
          ),
        ],

       ),
    );
  }
}

class _CustomRadialProgress extends StatelessWidget {
  const _CustomRadialProgress({

    @required this.porcentaje,
    @required this.color,
    @required this.grosorPrimario,
    @required this.grosorSecundario,
  });

  final double porcentaje;
  final Color color;
  final double grosorPrimario;
  final double grosorSecundario;

  @override
  Widget build(BuildContext context) {

      final appTheme = Provider.of<ThemeChanger>(context).currentTheme;
      
    return Container(
      width: 150,
      height: 150,
      // color: Colors.red,
      child: RadialProgress(
        porcentaje: porcentaje,
        color: color,
        colorSecundario: appTheme.textTheme.bodyText1.color,
        grosorPrimario: grosorPrimario,
        grosorSecundario: grosorSecundario,
      ),
    );
  }
}
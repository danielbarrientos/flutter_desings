import 'package:disenos/src/theme/theme.dart';
import 'package:disenos/src/widgets/pinterest_menu.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

class PinterestPage extends StatelessWidget {

  
  @override
  Widget build(BuildContext context) {

   
    
    return ChangeNotifierProvider(
      create:  (context) => new _MenuModel(),
      child: Scaffold(
   
        body: Stack(
          children: [
            PinterestGrid(),
            _PinterestMenuLocation()
          ],
        ),
   ),
    );
  }
}

class _PinterestMenuLocation extends StatelessWidget {
  

  @override
  Widget build(BuildContext context) {
    
    final widthScreen = MediaQuery.of(context).size.width;
    final bool show = Provider.of<_MenuModel>(context).show; 

    final appTheme = Provider.of<ThemeChanger>(context).currentTheme;
   

    return Positioned(
      bottom: 30,
      child: Container(
        width: widthScreen,
        child:  Align(
          alignment: Alignment.center,
          child: PinterestMenu(
            show: show,
            backgroundColor: appTheme.scaffoldBackgroundColor,
            activeColor: appTheme.accentColor,
            // inactiveColor: Colors.grey ,
            items: [
              PinterestButtom(icon: Icons.pie_chart, onPressed: (){print('Icon pie_chart');}),
              PinterestButtom(icon: Icons.search, onPressed: (){print('Icon search');}),
              PinterestButtom(icon: Icons.notifications, onPressed: (){print('Icon notifications');}),
              PinterestButtom(icon: Icons.supervised_user_circle, onPressed: (){print('Icon supervised_user_circle');}),
            ],
          ),
        ), 
      ),
    );
  }
}

class PinterestGrid extends StatefulWidget {

  @override
  _PinterestGridState createState() => _PinterestGridState();
}

class _PinterestGridState extends State<PinterestGrid> {


  final scrollController = ScrollController();
  double scrollAnterior = 0;

  final List<int> items = List.generate(200, (i) => i);

  @override
  void initState() {
    scrollController.addListener(() { 
      if(scrollController.offset> scrollAnterior && scrollController.offset>150){
        Provider.of<_MenuModel>(context,listen: false).show = false;
      }
      else{
         Provider.of<_MenuModel>(context,listen: false).show = true;
      }
      scrollAnterior= scrollController.offset;
    });
    super.initState();
    
    
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  } 

  @override
  Widget build(BuildContext context) {
    return new StaggeredGridView.countBuilder(
      controller: scrollController,
      crossAxisCount: 4,
      itemCount: items.length,
      itemBuilder: (BuildContext context, int index) => _PinterestItem(index),
      staggeredTileBuilder: (int index) =>
          new StaggeredTile.count(2, index.isEven ? 2 : 3),
      mainAxisSpacing: 4.0,
      crossAxisSpacing: 4.0,
    );
  }
}

class _PinterestItem extends StatelessWidget {

  final int index;

  const _PinterestItem(
    this.index,
  );

  @override
  Widget build(BuildContext context) {
    // return Container(
    //   margin: EdgeInsets.all(5),
    //   decoration: BoxDecoration(
    //     color: Colors.blue,
    //     borderRadius: BorderRadius.all(Radius.circular(20))
    //   ),
    //   child:  Center(
    //     child:  CircleAvatar(
    //       backgroundColor: Colors.white,
    //       child:  Text('$index'),
    //     ),
    //   )
    // );

    return Container(
      margin: EdgeInsets.all(5),
      decoration: BoxDecoration(
        color: Colors.purple[100],
        borderRadius: BorderRadius.all(Radius.circular(20))
      ),
      child: SvgPicture.asset('assets/svg/slide4.svg')
    );
    
  }
}

class _MenuModel with ChangeNotifier {
  bool _show = true;
 

  bool get show => this._show;

  set show (bool value){
    this._show = value;

    notifyListeners();
  }
}